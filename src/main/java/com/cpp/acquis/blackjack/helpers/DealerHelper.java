package com.cpp.acquis.blackjack.helpers;

import com.cpp.acquis.blackjack.exception.RandomNumberGenerationException;
import com.cpp.acquis.blackjack.model.Card;
import com.cpp.acquis.blackjack.model.InGameCard;
import com.cpp.acquis.blackjack.model.SuitEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.LongStream;


/**
 * Non house rule depending operations handler
 */
@Service
@Slf4j
public class DealerHelper {

    private Random random;

    public DealerHelper() {
        random = new Random();
    }

    public List<Card> createDeck() {
        ArrayList<Card> result = new ArrayList<>();
        for (SuitEnum suit : SuitEnum.values()) {
            result.add(new Card(1, "2", Arrays.asList(2), suit));
            result.add(new Card(2, "3", Arrays.asList(3), suit));
            result.add(new Card(3, "4", Arrays.asList(4), suit));
            result.add(new Card(4, "5", Arrays.asList(5), suit));
            result.add(new Card(5, "6", Arrays.asList(6), suit));
            result.add(new Card(6, "7", Arrays.asList(7), suit));
            result.add(new Card(7, "8", Arrays.asList(8), suit));
            result.add(new Card(8, "9", Arrays.asList(9), suit));
            result.add(new Card(9, "10", Arrays.asList(10), suit));
            result.add(new Card(10, "J", Arrays.asList(10), suit));
            result.add(new Card(11, "Q", Arrays.asList(10), suit));
            result.add(new Card(12, "K", Arrays.asList(10), suit));
            result.add(new Card(13, "A", Arrays.asList(1, 11), suit));
        }

        return result;
    }

    public List<Card> createDecks(int numDecks) {
        ArrayList<Card> result = new ArrayList<>();

        for (int i = 0; i < numDecks; i++)
            result.addAll(this.createDeck());

        return result;
    }

    public void shuffle(List<Card> decks) {
        Collections.shuffle(decks);
    }

    public Integer getRandom(int start, int end) throws RandomNumberGenerationException {
        try {
            OptionalInt oInt = random.ints(start, end).limit(1).findFirst();
            if (oInt.isPresent())
                return oInt.getAsInt();
        } catch (IllegalArgumentException e) {
            log.error(e.getMessage());
        }
        throw new RandomNumberGenerationException();
    }

    /**
     * Calculate the value of a specific set of cards
     *
     * @param cards
     * @return
     */
    public Integer getValue(List<InGameCard> cards) {
        List<Integer> possibleValues = getPossibleValues(cards);
        Collections.sort(possibleValues, Collections.reverseOrder());
        Optional<Integer> result = possibleValues.stream().filter(v -> v <= 21).findFirst();
        if (result.isPresent()) return result.get();
        else return null;
    }

    /**
     * Calculate the minimum value for a specific set of cards
     *
     * @param cards
     * @return
     */
    public Integer getMinValue(List<InGameCard> cards) {
        List<Integer> possibleValues = getPossibleValues(cards);
        Collections.sort(possibleValues);
        Optional<Integer> result = possibleValues.stream().findFirst();
        if (result.isPresent()) return result.get();
        else return null;
    }

    /**
     * Calculate all the possible values for a specific set of cards
     *
     * @param cards
     * @return
     */
    public List<Integer> getPossibleValues(List<InGameCard> cards) {
        int fixedValue = cards.stream().filter(c -> c.getCard().getValue().size() == 1).mapToInt(c -> c.getCard().getValue().get(0)).sum();
        long acesNumber = cards.stream().filter(c -> c.getCard().getValue().size() == 2).count();

        return acesValues(fixedValue, (int) acesNumber);
    }

    private List<Integer> acesValues(Integer fixed, int dept) {
        List<Integer> result = Arrays.asList(fixed);
        if (dept == 0) {
            return result;
        } else {
            return acesValuesInner(result, dept);
        }

    }

    private List<Integer> acesValuesInner(List<Integer> acc, int dept) {
        List<Integer> result = new ArrayList<>();
        acc.stream().forEach(v -> {
            result.add(v + 1);
            result.add(v + 11);
        });

        if (dept == 1) {
            return result;
        } else {
            return acesValuesInner(result, dept - 1);
        }
    }

}
