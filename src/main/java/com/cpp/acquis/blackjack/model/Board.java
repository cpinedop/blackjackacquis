package com.cpp.acquis.blackjack.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Board {
    private List<InGameCard> dealerSlot;
    private List<BoardSlot> playerSlots;

    public Board(){
        dealerSlot = new ArrayList<>();
        playerSlots = new ArrayList<>();
        playerSlots.add(new BoardSlot());
    }

    public void clean() {
        dealerSlot = new ArrayList<>();
        playerSlots = new ArrayList<>();
        playerSlots.add(new BoardSlot());
    }

}
