package com.cpp.acquis.blackjack.model;

import lombok.Data;

@Data
public class Round {
    private Integer slot;

    public Round(){
        this.setSlot(0);
    }

}
