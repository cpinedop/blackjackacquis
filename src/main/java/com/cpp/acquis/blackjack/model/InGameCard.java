package com.cpp.acquis.blackjack.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class InGameCard implements Comparable<InGameCard> {
    private Card card;
    private boolean hidden;

    @Override
    public String toString() {
        return !this.isHidden() ? card.getFigure().concat(card.getSuit().symbol()) : "?";
    }

    @Override
    public int compareTo(InGameCard inGameCard) {
        return card.getOrder().compareTo(inGameCard.getCard().getOrder());
    }
}
