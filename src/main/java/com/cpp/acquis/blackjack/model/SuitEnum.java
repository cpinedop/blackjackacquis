package com.cpp.acquis.blackjack.model;

public enum SuitEnum {
    SPADES("♠"), HEARTS("♥"), DIAMS("♦"), CLUBS("♣");

    private final String symbol;

    SuitEnum(String symbol) {
        this.symbol = symbol;
    }

    public String symbol() {
        return symbol;
    }
}
