package com.cpp.acquis.blackjack.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Player {
    private String name;
    private Long founds;

    public void removeFounds(Long amount) {
        this.founds -= amount;
    }

    public void addFounds(Long amount) {
        this.founds += amount;
    }
}
