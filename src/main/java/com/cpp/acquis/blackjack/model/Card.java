package com.cpp.acquis.blackjack.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
public class Card {
    private Integer order;
    private String figure;
    private List<Integer> value;
    private SuitEnum suit;
}
