package com.cpp.acquis.blackjack.model;

public enum GameStatusEnum {
    MENU(0), STARTED(1), INSURANCE(2), PLAYER_TURN(3);

    private final int order;

    GameStatusEnum(int order) {
        this.order = order;
    }

    public int order() {
        return order;
    }
}
