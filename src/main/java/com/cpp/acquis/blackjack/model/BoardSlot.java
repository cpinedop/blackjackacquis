package com.cpp.acquis.blackjack.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class BoardSlot {
    private Long bet;
    private List<InGameCard> cards;
    private boolean closed;

    public BoardSlot(){
        cards = new ArrayList<>();
    }

}
