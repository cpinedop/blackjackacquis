package com.cpp.acquis.blackjack.model;

import lombok.Data;

@Data
public class Game {
    private GameStatusEnum status;
    private Player player;
    private Board board;
    private Round round;

    public Game(){
        this.status = GameStatusEnum.MENU;
        this.player = null;
        this.board = new Board();
        this.round = null;
    }

    public void clean(){
        this.getBoard().clean();
        if (this.getRound()!=null) this.getRound().setSlot(0);
        this.setPlayer(null);
    }
}
