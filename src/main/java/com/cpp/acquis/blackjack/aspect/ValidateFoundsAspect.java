package com.cpp.acquis.blackjack.aspect;

import com.cpp.acquis.blackjack.model.Game;
import com.cpp.acquis.blackjack.service.impl.ConsoleWriterService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

@Aspect
@Configuration
public class ValidateFoundsAspect {
    public static final String NOT_ENOUGH_FOUNDS = "Not enough founds";
    @Resource
    private Game game;

    @Autowired
    private ConsoleWriterService console;

    /**
     * Check if the player has enough founds to perform the action denoted by the NeedFounds annotation
     */
    @Around("@annotation(com.cpp.acquis.blackjack.aspect.NeedFounds)")
    public void before(ProceedingJoinPoint jp) throws Throwable {
        Long foundsRequired = (Long) jp.getArgs()[0];
        if (foundsRequired <= game.getPlayer().getFounds()) jp.proceed();
        else console.write(NOT_ENOUGH_FOUNDS);
    }
}
