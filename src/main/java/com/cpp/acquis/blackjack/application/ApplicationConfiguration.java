package com.cpp.acquis.blackjack.application;

import com.cpp.acquis.blackjack.model.Card;
import com.cpp.acquis.blackjack.model.Game;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Stack;

/**
 * Bean configuration Class
 * */
@Configuration
public class ApplicationConfiguration {
    @Bean
    public Game game() {
        return new Game();
    }

    @Bean
    public Stack<Card> playableCards(){
        return new Stack<>();
    }

}
