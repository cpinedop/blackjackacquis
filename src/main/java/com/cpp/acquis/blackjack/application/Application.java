package com.cpp.acquis.blackjack.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.util.StringUtils;

@SpringBootApplication
@ComponentScan("com.cpp.acquis.blackjack")
public class Application {
    public static void main(String[] args) {
        String[] disabledCommands = {"--spring.shell.command.history.enabled=false", "--spring.shell.command.script.enabled=false"};
        String[] fullArgs = StringUtils.concatenateStringArrays(args, disabledCommands);
        SpringApplication.run(Application.class, fullArgs);
    }
}
