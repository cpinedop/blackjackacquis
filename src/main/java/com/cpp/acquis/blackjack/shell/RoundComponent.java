package com.cpp.acquis.blackjack.shell;

import com.cpp.acquis.blackjack.exception.BetUnderMinimumException;
import com.cpp.acquis.blackjack.exception.InsuranceNotValidException;
import com.cpp.acquis.blackjack.model.Game;
import com.cpp.acquis.blackjack.model.GameStatusEnum;
import com.cpp.acquis.blackjack.service.impl.BlackJackDealer;
import com.cpp.acquis.blackjack.service.impl.ConsoleWriterService;
import com.cpp.acquis.blackjack.service.impl.GameService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.Availability;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;


/**
 * Round shell component handler
 */
@ShellComponent("Round")
@Slf4j
public class RoundComponent {

    private static final String MINIMUM_BET_ERROR = "The minimum bet is %d$.";
    private static final String INSIRANCE_ERROR = "The minimum insurance is %d$.";

    @Autowired
    private Game game;

    @Autowired
    private ConsoleWriterService console;

    @Autowired
    private BlackJackDealer blackJackDealer;

    @Autowired
    private GameService gameService;

    private boolean isThisComponentAvailable() {
        return game.getStatus().order() > GameStatusEnum.MENU.order();
    }

    @ShellMethod(value = "BET", key = "b")
    public void bet(@ShellOption(defaultValue = "", help = "The default value is the minimum bet") Long value) throws BetUnderMinimumException {
        if (value == null)
            value = blackJackDealer.minimumBet();

        if (value < blackJackDealer.minimumBet()) {
            console.write(String.format(MINIMUM_BET_ERROR, blackJackDealer.minimumBet()));
            throw new BetUnderMinimumException();
        }


        gameService.startRound(value);
    }

    Availability betAvailability() {
        return isThisComponentAvailable() && game.getStatus().equals(GameStatusEnum.STARTED) ? Availability.available() : Availability.unavailable("the game has not started");
    }

    @ShellMethod(value = "INSURANCE, it is only available when the value of the first card of the dealer is 10 or higher", key = "i")
    public void insurance(@ShellOption(defaultValue = "", help = "The default value is the minimum insurance (0.5*bet)") Long value) throws InsuranceNotValidException {
        Long insurance;
        if (value == null) insurance = game.getBoard().getPlayerSlots().get(0).getBet() / 2;
        else if (value > game.getBoard().getPlayerSlots().get(0).getBet() / 2) insurance = value;
        else {
            console.write(String.format(INSIRANCE_ERROR, game.getBoard().getPlayerSlots().get(0).getBet() / 2));
            throw new InsuranceNotValidException();
        }

        console.write("Insurance bet %d", insurance);

        gameService.insurance(insurance);
    }

    Availability insuranceAvailability() {
        return isThisComponentAvailable() && game.getStatus().equals(GameStatusEnum.INSURANCE) ? Availability.available() : Availability.unavailable("insurance is not available");
    }

    @ShellMethod(value = "HIT", key = "h")
    public void hit() {
        gameService.hit();
    }

    Availability hitAvailability() {
        return isThisComponentAvailable() && game.getStatus().order() > GameStatusEnum.STARTED.order() ? Availability.available() : Availability.unavailable("hit is not available");
    }

    @ShellMethod(value = "STAND", key = "s")
    public void stand() {
        gameService.stand();
    }

    Availability standAvailability() {
        return isThisComponentAvailable() && game.getStatus().order() > GameStatusEnum.STARTED.order() ? Availability.available() : Availability.unavailable("stand is not available");
    }

    @ShellMethod(value = "DOUBLE DOWN, it is only available for the first action of the player", key = "d")
    public void doubleDown() {
        Long currentBet = game.getBoard().getPlayerSlots().get(game.getRound().getSlot()).getBet();
        gameService.doubleDown(currentBet);
    }

    Availability doubleDownAvailability() {
        return isThisComponentAvailable() && game.getStatus().order() > GameStatusEnum.STARTED.order() && blackJackDealer.acceptsDoubleDown() ? Availability.available() : Availability.unavailable("stand is not available");
    }

    @ShellMethod(value = "SPLIT, it is only available when the first two cards of the player are the same figure", key = "ss")
    public void split() {
        Integer slot = game.getRound().getSlot();
        gameService.split(game.getBoard().getPlayerSlots().get(slot).getBet());
    }

    Availability splitAvailability() {
        return isThisComponentAvailable() && game.getStatus().order() > GameStatusEnum.STARTED.order() && blackJackDealer.acceptsSplit() ? Availability.available() : Availability.unavailable("stand is not available");
    }

    @ShellMethod(value = "SURRENDER, it is only available for the first action of the player", key = "sr")
    public void surrender() {
        gameService.surrender();
    }

    Availability surrenderAvailability() {
        return isThisComponentAvailable() && game.getStatus().order() > GameStatusEnum.STARTED.order() && blackJackDealer.acceptsSurrender() ? Availability.available() : Availability.unavailable("stand is not available");
    }

    @ShellMethod("SHOW, Print the Board")
    public void show() {
        gameService.printGameStatus();
    }

    Availability showAvailability() {
        return isThisComponentAvailable() && game.getStatus().order() > GameStatusEnum.STARTED.order() ? Availability.available() : Availability.unavailable("the round has not started");
    }

}
