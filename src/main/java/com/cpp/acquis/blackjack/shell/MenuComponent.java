package com.cpp.acquis.blackjack.shell;

import com.cpp.acquis.blackjack.model.*;
import com.cpp.acquis.blackjack.service.IConsoleWriterService;
import com.cpp.acquis.blackjack.service.impl.GameService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.Availability;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import javax.annotation.Resource;
import java.nio.charset.Charset;

/**
 * Menu shell component handler
 */
@ShellComponent("Menu")
@Slf4j
public class MenuComponent {
    private static final String DEFAULT_NAME = "default player";
    private static final String DEFAULT_FOUNDS = "300";
    private static final String INSUFFICIENT_FOUNDS = "Not enough founds to start a game";

    @Autowired
    IConsoleWriterService console;

    @Autowired
    private Game game;

    @Autowired
    private GameService gameService;

    public MenuComponent() {
    }

    @ShellMethod("Start a new game, name and initial founds are configurable")
    public void start(@ShellOption(defaultValue = DEFAULT_NAME, help = "name of the player") String name, @ShellOption(defaultValue = DEFAULT_FOUNDS, help = "initial founds") Long founds) {
        if(founds>10L)
            gameService.startGame(name, founds);
        else
            console.write(INSUFFICIENT_FOUNDS);
    }

    Availability startAvailability() {
        return game.getStatus().equals(GameStatusEnum.MENU) ? Availability.available() : Availability.unavailable("the game has already started");
    }

    @ShellMethod("Finish the game")
    public void end() {
        gameService.endGame();
    }

    Availability endGameAvailability() {
        return game.getStatus().equals(GameStatusEnum.STARTED) ? Availability.available() : Availability.unavailable("the game has not started yet");
    }
}
