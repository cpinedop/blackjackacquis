package com.cpp.acquis.blackjack.shell;

import com.cpp.acquis.blackjack.model.Game;
import org.jline.utils.AttributedString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.jline.PromptProvider;
import org.springframework.stereotype.Component;

/**
 * Prompt provider
 */
@Component
public class BlackJackPromptProvider implements PromptProvider {

    private static final String MENU = "Menu> ";
    private static final String STARTED = "Playing as %s, founds %d$ > ";
    private static final String ROUND = "Founds %d$> ";

    @Autowired
    private Game game;

    @Override
    public AttributedString getPrompt() {
        switch (game.getStatus()) {
            case MENU:
                return new AttributedString(MENU);
            case STARTED:
                String startedPrompt = String.format(STARTED, game.getPlayer().getName(), game.getPlayer().getFounds());
                return new AttributedString(startedPrompt);
            default:
                String roundPrompt = String.format(ROUND, game.getPlayer().getFounds());
                return new AttributedString(roundPrompt);
        }
    }
}
