package com.cpp.acquis.blackjack.service.impl;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("EuropeanBlackJackDealer")
public class EuropeanBlackJackDealer extends BlackJackDealer {
    private static final boolean INSURANCE_AVAILABLE = true;
    private static final long INSURANCE_WAGE_RATIO = 4L;
    private static final int DECK_POOL_SIZE = 3;
    private static final int STAND_CONDITION = 17;
    private static final long STANDARD_WIN_RATIO = 2;
    private static final double BLACK_JACK_WIN_RATIO = 2.5;
    private static final long MINIMUM_BET = 10;
    private static final boolean DOUBLE_DOWN_AVAILABLE = true;
    private static final boolean SPLIT_AVAILABLE = true;
    private static final boolean SURRENDER_AVAILABLE = true;
    private static final int MAX_SPLIT_SLOTS = 4;

    @Override
    public boolean insuranceAvailable() {
        return INSURANCE_AVAILABLE;
    }

    @Override
    public long insuranceWageRatio() {
        return INSURANCE_WAGE_RATIO;
    }

    @Override
    public int deckPoolSize() {
        return DECK_POOL_SIZE;
    }

    @Override
    public int standCondition() {
        return STAND_CONDITION;
    }

    @Override
    public long standardWinRatio() {
        return STANDARD_WIN_RATIO;
    }

    @Override
    public double blackJackWinRatio() {
        return BLACK_JACK_WIN_RATIO;
    }

    @Override
    public long minimumBet() {
        return MINIMUM_BET;
    }

    @Override
    public boolean doubleDownAvailable() {
        return DOUBLE_DOWN_AVAILABLE;
    }

    @Override
    public boolean splitAvailable() {
        return SPLIT_AVAILABLE;
    }

    @Override
    public boolean surrenderAvailable() {
        return SURRENDER_AVAILABLE;
    }

    @Override
    public int maxSplitSlots() {
        return MAX_SPLIT_SLOTS;
    }
}
