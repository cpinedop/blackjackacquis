package com.cpp.acquis.blackjack.service;

public interface IConsoleWriterService {
    void write(String msg, Object... args);
}
