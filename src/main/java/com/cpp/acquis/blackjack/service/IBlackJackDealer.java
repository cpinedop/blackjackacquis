package com.cpp.acquis.blackjack.service;

import com.cpp.acquis.blackjack.exception.RandomNumberGenerationException;
import com.cpp.acquis.blackjack.model.InGameCard;

import java.util.List;

/**
 * Handle the dealer actions
 * */
public interface IBlackJackDealer {
    void startRound() throws RandomNumberGenerationException;

    boolean isInsuranceAvailable();

    boolean checkDealerBlackjack(boolean hidden);

    Integer getValue(List<InGameCard> cards);

    void performHit();

    boolean checkBust();

    boolean checkAutoStand();

    void play();

    void push();

    void closeSlot(Integer slot);

    void playerWin();

    void playerLose();

    boolean checkPlayerBlackJack();

    void playerBlackJack();

    long playerInsuranceWin(long insurance);

    boolean acceptsDoubleDown();

    boolean acceptsSplit();

    boolean acceptsSurrender();

    void playerSurrender();

    void performSplit(List<Integer> indexes);
}
