package com.cpp.acquis.blackjack.service.impl;

import com.cpp.acquis.blackjack.exception.RandomNumberGenerationException;
import com.cpp.acquis.blackjack.model.Card;
import com.cpp.acquis.blackjack.model.Game;
import com.cpp.acquis.blackjack.model.InGameCard;
import com.cpp.acquis.blackjack.helpers.DealerHelper;
import com.cpp.acquis.blackjack.service.IBlackJackDealer;
import com.cpp.acquis.blackjack.service.IConsoleWriterService;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.List;
import java.util.Stack;

public abstract class BlackJackDealer implements IBlackJackDealer {
    @Autowired
    private Game game;

    @Autowired
    DealerHelper dealerHelper;

    @Autowired
    IConsoleWriterService console;

    @Autowired
    Stack<Card> playableCards;

    public abstract boolean insuranceAvailable();

    public abstract long insuranceWageRatio();

    public abstract int deckPoolSize();

    public abstract int standCondition();

    public abstract long standardWinRatio();

    public abstract double blackJackWinRatio();

    public abstract long minimumBet();

    public abstract boolean doubleDownAvailable();

    public abstract boolean splitAvailable();

    public abstract boolean surrenderAvailable();

    public abstract int maxSplitSlots();

    @Override
    public void startRound() throws RandomNumberGenerationException {
        List<Card> decks = dealerHelper.createDecks(deckPoolSize());
        dealerHelper.shuffle(decks);
        decks.subList(0, dealerHelper.getRandom(decks.size() / 2, decks.size())).stream().forEach(playableCards::push);

        game.getBoard().getPlayerSlots().get(0).getCards().add(new InGameCard(playableCards.pop(), false));
        game.getBoard().getDealerSlot().add(new InGameCard(playableCards.pop(), false));
        game.getBoard().getPlayerSlots().get(0).getCards().add(new InGameCard(playableCards.pop(), false));
    }

    @Override
    public boolean isInsuranceAvailable() {
        Integer dealerValue = dealerHelper.getValue(game.getBoard().getDealerSlot());
        return insuranceAvailable() && dealerValue != null && dealerValue >= 10;
    }

    @Override
    public boolean checkDealerBlackjack(boolean hidden) {
        game.getBoard().getDealerSlot().add(new InGameCard(playableCards.pop(), hidden));
        Integer value = dealerHelper.getValue(game.getBoard().getDealerSlot());
        return value != null && value == 21;
    }

    @Override
    public Integer getValue(List<InGameCard> cards) {
        return dealerHelper.getValue(cards);
    }

    @Override
    public void performHit() {
        Integer slot = game.getRound().getSlot();
        game.getBoard().getPlayerSlots().get(slot).getCards().add(new InGameCard(playableCards.pop(), false));
    }

    @Override
    public boolean checkBust() {
        Integer slot = game.getRound().getSlot();
        Integer value = this.getValue(game.getBoard().getPlayerSlots().get(slot).getCards());
        return value == null;
    }

    @Override
    public boolean checkAutoStand() {
        Integer slot = game.getRound().getSlot();
        Integer value = this.getValue(game.getBoard().getPlayerSlots().get(slot).getCards());
        return value != null && value == 21;
    }

    @Override
    public void play() {
        while (dealerHelper.getValue(game.getBoard().getDealerSlot()) != null && dealerHelper.getMinValue(game.getBoard().getDealerSlot()) < standCondition()) {
            game.getBoard().getDealerSlot().add(new InGameCard(playableCards.pop(), false));
        }
        game.getBoard().getDealerSlot().stream().forEach(c -> c.setHidden(false));
    }

    @Override
    public void push() {
        Integer slot = game.getRound().getSlot();
        long wage = game.getBoard().getPlayerSlots().get(slot).getBet();
        game.getPlayer().addFounds(wage);
        this.closeSlot(slot);
        console.write("Push +%d$", wage);
    }

    @Override
    public void closeSlot(Integer slot) {
        game.getBoard().getPlayerSlots().get(slot).setClosed(true);
    }

    @Override
    public void playerWin() {
        Integer slot = game.getRound().getSlot();
        long wage = game.getBoard().getPlayerSlots().get(slot).getBet() * standardWinRatio();
        game.getPlayer().addFounds(wage);
        this.closeSlot(slot);
        console.write("%s win +%d$", game.getPlayer().getName(), wage);
    }

    @Override
    public void playerLose() {
        Integer slot = game.getRound().getSlot();
        this.closeSlot(slot);
        console.write("%s loses", game.getPlayer().getName());
    }

    @Override
    public boolean checkPlayerBlackJack() {
        Integer value = this.getValue(game.getBoard().getPlayerSlots().get(0).getCards());
        return value != null && value == 21;
    }

    @Override
    public void playerBlackJack() {
        BigDecimal bet = new BigDecimal(game.getBoard().getPlayerSlots().get(0).getBet());
        BigDecimal wage = bet.multiply(new BigDecimal(blackJackWinRatio()));
        game.getPlayer().addFounds(wage.longValue());
        console.write("BlackJack! %s wins", game.getPlayer().getName());
    }

    @Override
    public long playerInsuranceWin(long insurance) {
        Long wage = insurance * insuranceWageRatio();
        game.getPlayer().addFounds(wage);
        return wage;
    }

    @Override
    public boolean acceptsDoubleDown() {
        if (!doubleDownAvailable()) return false;
        Integer slot = game.getRound().getSlot();
        if (game.getBoard().getPlayerSlots().get(slot).getCards().size() != 2) return false;
        return true;
    }

    @Override
    public boolean acceptsSplit() {
        if (!splitAvailable()) return false;
        if (game.getBoard().getPlayerSlots().size() >= maxSplitSlots()) return false;
        Integer slot = game.getRound().getSlot();
        if (game.getBoard().getPlayerSlots().get(slot).getCards().size() != 2) return false;

        Card firstCard = game.getBoard().getPlayerSlots().get(slot).getCards().get(0).getCard();
        Card secondCard = game.getBoard().getPlayerSlots().get(slot).getCards().get(1).getCard();

        if (firstCard.getFigure().equals(secondCard.getFigure())) return true;
        return false;
    }

    @Override
    public boolean acceptsSurrender() {
        if (!surrenderAvailable()) return false;
        Integer slot = game.getRound().getSlot();
        if (game.getBoard().getPlayerSlots().get(slot).getCards().size() != 2) return false;
        if (game.getBoard().getDealerSlot().size() > 1) return false;
        return true;
    }

    @Override
    public void playerSurrender() {
        long wage = game.getBoard().getPlayerSlots().get(0).getBet() / 2;
        game.getPlayer().addFounds(wage);
    }

    @Override
    public void performSplit(List<Integer> indexes) {
        InGameCard card2 = game.getBoard().getPlayerSlots().get(indexes.get(0)).getCards().get(1);

        game.getBoard().getPlayerSlots().get(indexes.get(0)).getCards().remove(card2);
        game.getBoard().getPlayerSlots().get(indexes.get(1)).getCards().add(card2);

        game.getBoard().getPlayerSlots().get(indexes.get(0)).getCards().add(new InGameCard(playableCards.pop(), false));
        game.getBoard().getPlayerSlots().get(indexes.get(1)).getCards().add(new InGameCard(playableCards.pop(), false));
    }
}
