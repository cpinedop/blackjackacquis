package com.cpp.acquis.blackjack.service;

import com.cpp.acquis.blackjack.aspect.NeedFounds;
import com.cpp.acquis.blackjack.exception.InsuranceNotValidException;

/**
 * Game controller, it handles the game actions
 * */
public interface IGameService {
    void printGameStatus();

    void stand();

    void startRound(Long bet);

    void endGame();

    void startGame(String name, Long founds);

    void insurance(Long insurance);

    void hit();

    void doubleDown(Long bet);

    void surrender();

    void split(Long bet);
}
