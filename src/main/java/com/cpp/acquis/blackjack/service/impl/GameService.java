package com.cpp.acquis.blackjack.service.impl;

import com.cpp.acquis.blackjack.aspect.NeedFounds;
import com.cpp.acquis.blackjack.exception.RandomNumberGenerationException;
import com.cpp.acquis.blackjack.model.*;
import com.cpp.acquis.blackjack.service.IBlackJackDealer;
import com.cpp.acquis.blackjack.service.IConsoleWriterService;
import com.cpp.acquis.blackjack.service.IGameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

@Service
public class GameService implements IGameService {
    @Resource
    private Game game;

    @Autowired
    @Qualifier("EuropeanBlackJackDealer")
    private IBlackJackDealer dealer;

    @Autowired
    private IConsoleWriterService console;

    @Override
    public void startGame(String name, Long founds) {
        game.setStatus(GameStatusEnum.STARTED);
        game.setPlayer(new Player(name, founds));
        console.write("Starting game as %s with %d$", game.getPlayer().getName(), game.getPlayer().getFounds());
    }

    @Override
    public void endGame() {
        game.setStatus(GameStatusEnum.MENU);
        console.write("End game %s final founds %d$", game.getPlayer().getName(), game.getPlayer().getFounds());
        game.clean();
    }

    @Override
    public void printGameStatus() {
        console.write("Dealer %s", game.getBoard().getDealerSlot().stream().map(InGameCard::toString).collect(Collectors.joining(" ")));
        for(int i=0;i<game.getBoard().getPlayerSlots().size();i++) {
            String activeSlot = game.getRound().getSlot().intValue() == i ? "*" : "";
            BoardSlot bs = game.getBoard().getPlayerSlots().get(i);
            String closePendingStr = bs.isClosed() ? "closed" : "pending";
            String cardsStr = bs.getCards().stream().map(InGameCard::toString).collect(Collectors.joining(" "));
            console.write("%s%s %s bet %s$ %s", activeSlot, game.getPlayer().getName(), cardsStr, bs.getBet(), closePendingStr);
        }
    }

    @Override
    public void stand() {
        Integer slot = game.getRound().getSlot();
        game.getBoard().getPlayerSlots().get(slot).setClosed(true);
        if (!nextSlot()) {
            dealer.play();
            printGameStatus();
            resolveSlots();
            endRound();
            console.write("Round end");
        }
    }

    private void resolveSlots() {
        for (int i = 0; i < game.getBoard().getPlayerSlots().size(); i++) {
            resolveSlot(i);
        }
    }

    private void resolveSlot(Integer slot) {
        Integer dealerScore = dealer.getValue(game.getBoard().getDealerSlot());
        Integer slotScore = dealer.getValue(game.getBoard().getPlayerSlots().get(slot).getCards());

        if (dealerScore == null) dealerScore = 0;
        if (slotScore == null) slotScore = 0;

        if (dealerScore == slotScore) {
            dealer.push();
        } else if (dealerScore > slotScore) {
            dealer.playerLose();
        } else if (dealerScore < slotScore) {
            dealer.playerWin();
        }
    }

    private boolean nextSlot() {
        boolean found = false;
        for (int i = 0; i < game.getBoard().getPlayerSlots().size(); i++) {
            if (!game.getBoard().getPlayerSlots().get(i).isClosed()) {
                game.getRound().setSlot(i);
                found = true;
                break;
            }
        }
        return found;
    }

    @Override
    @NeedFounds
    public void startRound(Long bet) {
        console.write("%s bet %d$", game.getPlayer().getName(), bet);
        game.setRound(new Round());
        game.getPlayer().removeFounds(bet);
        game.getBoard().getPlayerSlots().get(0).setBet(bet);
        try {
            dealer.startRound();
            if (dealer.isInsuranceAvailable())
                game.setStatus(GameStatusEnum.INSURANCE);
            else
                game.setStatus(GameStatusEnum.PLAYER_TURN);
            this.printGameStatus();

            if (dealer.checkPlayerBlackJack()) {
                if(dealer.checkDealerBlackjack(false)){
                    this.printGameStatus();
                    dealer.push();
                }else {
                    this.printGameStatus();
                    dealer.playerBlackJack();
                }
                endRound();
            }
        } catch (RandomNumberGenerationException e) {
            console.write("The dealer has made a mistake the round is cancelled");
            game.getPlayer().addFounds(game.getBoard().getPlayerSlots().get(0).getBet());
            endRound();
        }
    }

    @Override
    @NeedFounds
    public void insurance(Long insurance) {
        game.getPlayer().removeFounds(insurance);
        if (dealer.checkDealerBlackjack(true)) {
            long wage = dealer.playerInsuranceWin(insurance);
            console.write("The dealer has BlackJack! Insurance win. Wage: %d$", wage);
            console.write("%s founds: %d$", game.getPlayer().getName(), game.getPlayer().getFounds());

            endRound();
        } else {
            console.write("The dealer has not BlackJack, insurance lost");
            game.setStatus(GameStatusEnum.PLAYER_TURN);
            printGameStatus();
        }
    }

    private void endRound() {
        game.getBoard().clean();
        game.setRound(null);
        game.setStatus(GameStatusEnum.STARTED);
    }

    @Override
    public void hit() {
        dealer.performHit();
        printGameStatus();
        if (dealer.checkBust()) {
            console.write("Bust, bet lost");
            game.getBoard().getPlayerSlots().get(game.getRound().getSlot()).setCards(new ArrayList<>());
            game.getBoard().getPlayerSlots().get(game.getRound().getSlot()).setBet(0L);
            this.stand();
        } else if (dealer.checkAutoStand()) {
            console.write("21 automatic stand...");
            this.stand();
        }
    }

    @Override
    @NeedFounds
    public void doubleDown(Long bet){
        Integer slot = game.getRound().getSlot();
        game.getPlayer().removeFounds(bet);
        game.getBoard().getPlayerSlots().get(slot).setBet(bet*2);
        this.hit();
        if(!game.getStatus().equals(GameStatusEnum.STARTED)) {
            this.stand();
        }
    }

    @Override
    public void surrender() {
        console.write("Surrender");
        dealer.playerSurrender();
        this.endRound();
    }

    @Override
    @NeedFounds
    public void split(Long bet) {
        Integer slot = game.getRound().getSlot();
        game.getPlayer().removeFounds(bet);
        BoardSlot newSlot = new BoardSlot();
        newSlot.setBet(game.getBoard().getPlayerSlots().get(slot).getBet());
        Integer newSlotIndex = game.getBoard().getPlayerSlots().size();
        game.getBoard().getPlayerSlots().add(newSlot);
        dealer.performSplit(Arrays.asList(slot, newSlotIndex));
        this.printGameStatus();
    }
}
