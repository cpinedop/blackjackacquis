package com.cpp.acquis.blackjack.service.impl;

import com.cpp.acquis.blackjack.service.IConsoleWriterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

@Service
@Slf4j
public class ConsoleWriterService implements IConsoleWriterService {

    PrintStream out;

    public ConsoleWriterService(){
        try {
            loadPrintStream();
        } catch (UnsupportedEncodingException e) {
            log.warn("Error creating PrintStream");
            out = System.out;
        }
    }

    private void loadPrintStream() throws UnsupportedEncodingException {
        out=new PrintStream(System.out, true, "UTF-8");
    }

    @Override
    public void write(String msg, Object... args) {
        out.print("> ");
        out.printf(msg, (Object[]) args);
        out.println();
    }
}
