package com.cpp.acquis.blackjack.helpers;

import com.cpp.acquis.blackjack.exception.RandomNumberGenerationException;
import com.cpp.acquis.blackjack.model.Card;
import com.cpp.acquis.blackjack.model.InGameCard;
import com.cpp.acquis.blackjack.model.SuitEnum;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class DealerHelperTest {

    @InjectMocks
    DealerHelper subject;

    @Test
    public void getPossibleValuesTest() {
        InGameCard card2 = new InGameCard(new Card(1, "2", Arrays.asList(2), SuitEnum.CLUBS), false);
        InGameCard cardAce = new InGameCard(new Card(13, "A", Arrays.asList(1, 14), SuitEnum.CLUBS), false);

        List<InGameCard> cards1 = Arrays.asList(card2, cardAce);
        List<Integer> values1 = subject.getPossibleValues(cards1);

        List<InGameCard> cards2 = Arrays.asList(card2);
        List<Integer> values2 = subject.getPossibleValues(cards2);

        List<InGameCard> cards3 = Arrays.asList(cardAce, cardAce);
        List<Integer> values3 = subject.getPossibleValues(cards3);

        Assert.assertEquals(Arrays.asList(3, 13), values1);
        Assert.assertEquals(Arrays.asList(2), values2);
        Assert.assertEquals(Arrays.asList(2, 12, 12, 22), values3);
    }

    @Test
    public void getValueTest() {
        InGameCard card2 = new InGameCard(new Card(1, "2", Arrays.asList(2), SuitEnum.CLUBS), false);
        InGameCard card9 = new InGameCard(new Card(8, "9", Arrays.asList(9), SuitEnum.CLUBS), false);
        InGameCard cardAce = new InGameCard(new Card(13, "A", Arrays.asList(1, 11), SuitEnum.CLUBS), false);
        InGameCard cardQ = new InGameCard(new Card(11, "Q", Arrays.asList(10), SuitEnum.CLUBS), false);

        List<InGameCard> cards1 = Arrays.asList(card2, cardAce);
        Integer values1 = subject.getValue(cards1);

        List<InGameCard> cards2 = Arrays.asList(card2);
        Integer values2 = subject.getValue(cards2);

        List<InGameCard> cards3 = Arrays.asList(cardAce, cardAce);
        Integer values3 = subject.getValue(cards3);

        List<InGameCard> cards4 = Arrays.asList(card9, card9, card9, card9, card9, cardAce, cardAce);
        Integer values4 = subject.getValue(cards4);

        List<InGameCard> cards5 = Arrays.asList(cardAce, cardQ);
        Integer values5 = subject.getValue(cards5);

        Assert.assertTrue(13 == values1);
        Assert.assertTrue(2 == values2);
        Assert.assertTrue(12 == values3);
        Assert.assertTrue(null == values4);
        Assert.assertTrue(21 == values5);
    }

    @Test
    public void createDeckTest() {
        List<Card> result = subject.createDeck();
        List<Card> expect = subject.createDeck();

        assertEquals(expect, result);
        assertEquals(52, result.size());
    }

    @Test
    public void createDecksTest() {
        List<Card> result1 = subject.createDecks(1);
        List<Card> expect1 = subject.createDeck();

        assertEquals(expect1, result1);
        assertEquals(52, result1.size());

        List<Card> result2 = subject.createDecks(2);
        List<Card> expect2 = subject.createDeck();
        expect2.addAll(subject.createDeck());

        assertEquals(expect2, result2);
        assertEquals(52 * 2, result2.size());
    }

    @Test
    public void shuffleTest() {
        List<Card> resultA = subject.createDecks(10);
        List<Card> resultB = subject.createDecks(10);

        while(resultA.get(0).getFigure() == resultB.get(0).getFigure()){
            subject.shuffle(resultB);
        }

        assertNotEquals(resultA.get(0), resultB.get(0));
    }

    @Test
    public void getRandomTest() throws RandomNumberGenerationException {
        Integer result = subject.getRandom(0,100);
        assertTrue(0 <= result && result <= 100);
    }

    @Test(expected = RandomNumberGenerationException.class)
    public void getRandomExceptionTest() throws RandomNumberGenerationException {
        Integer result = subject.getRandom(100,0);
    }

    @Test
    public void getMinValueTest() {
        InGameCard card2 = new InGameCard(new Card(1, "2", Arrays.asList(2), SuitEnum.CLUBS), false);
        InGameCard card9 = new InGameCard(new Card(8, "9", Arrays.asList(9), SuitEnum.CLUBS), false);
        InGameCard cardAce = new InGameCard(new Card(13, "A", Arrays.asList(1, 11), SuitEnum.CLUBS), false);
        InGameCard cardQ = new InGameCard(new Card(11, "Q", Arrays.asList(10), SuitEnum.CLUBS), false);

        List<InGameCard> cards = Arrays.asList(card2, cardAce);
        Integer value = subject.getMinValue(cards);
        Integer expected = 3;

        assertEquals(expected, value);

        cards = Arrays.asList(card2);
        value = subject.getMinValue(cards);
        expected = 2;

        assertEquals(expected, value);

        cards = Arrays.asList(card2, cardQ);
        value = subject.getMinValue(cards);
        expected = 12;

        assertEquals(expected, value);

        cards = Arrays.asList(cardAce, cardAce, cardAce);
        value = subject.getMinValue(cards);
        expected = 3;

        assertEquals(expected, value);
    }
}