package com.cpp.acquis.blackjack.aspect;

import com.cpp.acquis.blackjack.helpers.DealerHelper;
import com.cpp.acquis.blackjack.model.Game;
import com.cpp.acquis.blackjack.model.Player;
import com.cpp.acquis.blackjack.service.impl.ConsoleWriterService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ValidateFoundsAspectTest {

    @InjectMocks
    ValidateFoundsAspect subject;

    @Mock
    ProceedingJoinPoint pj;

    @Mock
    Game game;

    @Mock
    ConsoleWriterService console;

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));

        Mockito.doAnswer(new Answer(){
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                outContent.write(((String) invocation.getArgument(0)).getBytes());
                return null;
            }
        }).when(console).write(Mockito.anyString());
    }

    @Test
    public void beforeValidTest() throws Throwable {
        Mockito.when(pj.getArgs()).thenReturn(Arrays.asList(3L).toArray());
        Player player = new Player("test", 3L);
        Mockito.when(game.getPlayer()).thenReturn(player);

        subject.before(pj);
        assertEquals("", outContent.toString());

        player = new Player("test", 0L);
        Mockito.when(game.getPlayer()).thenReturn(player);
        subject.before(pj);
        assertEquals("Not enough founds", outContent.toString());
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }
}