package com.cpp.acquis.blackjack.service.impl;

import com.cpp.acquis.blackjack.helpers.DealerHelper;
import com.cpp.acquis.blackjack.model.*;
import com.cpp.acquis.blackjack.service.IBlackJackDealer;
import com.cpp.acquis.blackjack.service.IConsoleWriterService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class GameServiceTest {

    @InjectMocks
    GameService subject;

    @Mock
    IConsoleWriterService console;

    @Mock
    IBlackJackDealer dealer;

    @Mock
    Game game;

    Board board;

    Round round;

    Player player;

    @Before
    public void setUp() throws Exception {
        board = new Board();
        board.getPlayerSlots().get(0).setBet(20L);
        Mockito.doReturn(board).when(game).getBoard();
        round = new Round();
        round.setSlot(0);
        Mockito.doReturn(round).when(game).getRound();
        player = new Player("test", 400L);
        Mockito.doReturn(player).when(game).getPlayer();
        Mockito.doReturn(GameStatusEnum.MENU).when(game).getStatus();
    }

    @Test
    public void startGame() {
        subject.startGame("test", 300L);
        assertTrue(true);
    }

    @Test
    public void endGame() {
        subject.endGame();
        assertTrue(true);
    }

    @Test
    public void printGameStatus() {
        subject.printGameStatus();
        assertTrue(true);
    }

    @Test
    public void stand() {
        subject.stand();
        assertTrue(true);
    }

    @Test
    public void startRound() {
        subject.startRound(10L);
        assertTrue(true);
    }

    @Test
    public void insurance() {
        subject.insurance(10L);
        assertTrue(true);
    }

    @Test
    public void hit() {
        subject.hit();
        assertTrue(true);
    }

    @Test
    public void doubleDown() {
        subject.doubleDown(10L);
        assertTrue(true);
    }

    @Test
    public void surrender() {
        subject.surrender();
        assertTrue(true);
    }

    @Test
    public void split() {
        subject.split(10L);
        assertTrue(true);
    }
}