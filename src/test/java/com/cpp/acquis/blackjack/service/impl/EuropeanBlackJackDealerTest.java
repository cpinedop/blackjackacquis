package com.cpp.acquis.blackjack.service.impl;

import com.cpp.acquis.blackjack.exception.RandomNumberGenerationException;
import com.cpp.acquis.blackjack.helpers.DealerHelper;
import com.cpp.acquis.blackjack.model.*;
import com.cpp.acquis.blackjack.service.IConsoleWriterService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class EuropeanBlackJackDealerTest {

    @Spy
    @InjectMocks
    EuropeanBlackJackDealer subject;

    @Mock
    DealerHelper dealerHelper;

    @Mock
    Stack<Card> playableCards;

    @Mock
    IConsoleWriterService console;

    @Mock
    Game game;

    Board board;

    Round round;

    Player player;

    @Before
    public void setUp() throws Exception {
        board = new Board();
        board.getPlayerSlots().get(0).setBet(20L);
        Mockito.doReturn(board).when(game).getBoard();
        round = new Round();
        round.setSlot(0);
        Mockito.doReturn(round).when(game).getRound();
        player = new Player("test", 400L);
        Mockito.doReturn(player).when(game).getPlayer();

        Card card2 = new Card(1, "2", Arrays.asList(2), SuitEnum.CLUBS);
        Mockito.doReturn(card2).when(playableCards).pop();

        List<Card> gameDeck = new ArrayList<>();
        gameDeck.add(new Card(1, "2", Arrays.asList(2), SuitEnum.CLUBS));
        gameDeck.add(new Card(1, "2", Arrays.asList(2), SuitEnum.CLUBS));
        gameDeck.add(new Card(1, "2", Arrays.asList(2), SuitEnum.CLUBS));
        gameDeck.add(new Card(1, "2", Arrays.asList(2), SuitEnum.CLUBS));
        gameDeck.add(new Card(1, "2", Arrays.asList(2), SuitEnum.CLUBS));
        gameDeck.add(new Card(1, "2", Arrays.asList(2), SuitEnum.CLUBS));
        gameDeck.add(new Card(1, "2", Arrays.asList(2), SuitEnum.CLUBS));

        Mockito.doReturn(gameDeck).when(dealerHelper).createDecks(Mockito.anyInt());
    }

    @Test
    public void insuranceAvailable() {
        assertTrue(subject.insuranceAvailable());
    }

    @Test
    public void insuranceWageRatio() {
        assertEquals(4L, subject.insuranceWageRatio());
    }

    @Test
    public void deckPoolSize() {
        assertEquals(3, subject.deckPoolSize());
    }

    @Test
    public void standCondition() {
        assertEquals(17, subject.standCondition());
    }

    @Test
    public void standardWinRatio() {
        assertEquals(2, subject.standardWinRatio());
    }

    @Test
    public void blackJackdWinRatio() {
        assertEquals(2.5, subject.blackJackWinRatio(), 0);
    }

    @Test
    public void minimumBet() {
        assertEquals(10, subject.minimumBet());
    }

    @Test
    public void doubleDownAvailable() {
        assertTrue(subject.doubleDownAvailable());
    }

    @Test
    public void splitAvailable() {
        assertTrue(subject.splitAvailable());
    }

    @Test
    public void surrenderAvailable() {
        assertTrue(subject.surrenderAvailable());
    }

    @Test
    public void maxSplitSlots() {
        assertEquals(4, subject.maxSplitSlots());
    }

    @Test
    public void startRound() throws RandomNumberGenerationException {
        Mockito.doReturn(7).when(dealerHelper).getRandom(Mockito.anyInt(), Mockito.anyInt());

        subject.startRound();

        assertTrue(game.getBoard().getPlayerSlots().size() == 1);
        assertTrue(game.getBoard().getDealerSlot().size() == 1);
    }

    @Test
    public void isInsuranceAvailable() {
        Mockito.doReturn(board).when(game).getBoard();
        Mockito.doReturn(null).when(dealerHelper).getValue(Mockito.anyList());
        assertFalse(subject.isInsuranceAvailable());
        Mockito.doReturn(10).when(dealerHelper).getValue(Mockito.anyList());
        assertTrue(subject.isInsuranceAvailable());
        Mockito.doReturn(11).when(dealerHelper).getValue(Mockito.anyList());
        assertTrue(subject.isInsuranceAvailable());
        Mockito.doReturn(3).when(dealerHelper).getValue(Mockito.anyList());
        assertFalse(subject.isInsuranceAvailable());
    }

    @Test
    public void checkDealerBlackjack() {
        board = new Board();
        Mockito.doReturn(board).when(game).getBoard();
        Mockito.doReturn(2).when(dealerHelper).getValue(Mockito.anyList());

        assertFalse(subject.checkDealerBlackjack(true));

        Mockito.doReturn(21).when(dealerHelper).getValue(Mockito.anyList());
        assertTrue(subject.checkDealerBlackjack(true));
    }

    @Test
    public void getValue() {
        Integer expected = 2;
        Mockito.doReturn(2).when(dealerHelper).getValue(Mockito.anyList());
        assertEquals(expected, subject.getValue(new ArrayList<>()));
    }

    @Test
    public void performHit() {
        subject.performHit();
        assertTrue(true);
    }

    @Test
    public void checkBust() {
        Mockito.doReturn(2).when(dealerHelper).getValue(Mockito.anyList());
        assertFalse(subject.checkBust());

        Mockito.doReturn(null).when(dealerHelper).getValue(Mockito.anyList());
        assertTrue(subject.checkBust());

        Mockito.doReturn(21).when(dealerHelper).getValue(Mockito.anyList());
        assertFalse(subject.checkBust());
    }

    @Test
    public void checkAutoStand() {
        Mockito.doReturn(2).when(dealerHelper).getValue(Mockito.anyList());
        assertFalse(subject.checkAutoStand());

        Mockito.doReturn(null).when(dealerHelper).getValue(Mockito.anyList());
        assertFalse(subject.checkAutoStand());

        Mockito.doReturn(21).when(dealerHelper).getValue(Mockito.anyList());
        assertTrue(subject.checkAutoStand());
    }

    @Test
    public void play() {
        Mockito.doReturn(2).doReturn(null).when(dealerHelper).getValue(Mockito.anyList());

        subject.play();
        assertTrue(true);
    }

    @Test
    public void push() {
        subject.push();
        assertTrue(true);
    }

    @Test
    public void closeSlot() {
        subject.closeSlot(0);
        assertTrue(true);
    }

    @Test
    public void playerWin() {
        subject.playerWin();
        assertTrue(true);
    }

    @Test
    public void playerLose() {
        subject.playerLose();
        assertTrue(true);
    }

    @Test
    public void checkPlayerBlackJack() {
        Mockito.doReturn(2).when(dealerHelper).getValue(Mockito.anyList());
        assertFalse(subject.checkPlayerBlackJack());

        Mockito.doReturn(null).when(dealerHelper).getValue(Mockito.anyList());
        assertFalse(subject.checkPlayerBlackJack());

        Mockito.doReturn(21).when(dealerHelper).getValue(Mockito.anyList());
        assertTrue(subject.checkPlayerBlackJack());
    }

    @Test
    public void playerBlackJack() {
        subject.playerBlackJack();
        assertTrue(true);
    }

    @Test
    public void playerInsuranceWin() {
        subject.playerInsuranceWin(3L);
        assertTrue(true);
    }

    @Test
    public void acceptsDoubleDown() {
        Mockito.doReturn(false).when(subject).doubleDownAvailable();
        assertFalse(subject.acceptsDoubleDown());
        Mockito.doReturn(true).when(subject).doubleDownAvailable();
        assertFalse(subject.acceptsDoubleDown());
        board.getPlayerSlots().get(0).getCards().add(new InGameCard(new Card(1, "2", Arrays.asList(2), SuitEnum.CLUBS), true));
        board.getPlayerSlots().get(0).getCards().add(new InGameCard(new Card(1, "2", Arrays.asList(2), SuitEnum.CLUBS), true));
        assertTrue(subject.acceptsDoubleDown());
    }

    @Test
    public void acceptsSplit() {
        Mockito.doReturn(false).when(subject).splitAvailable();
        assertFalse(subject.acceptsSplit());
        Mockito.doReturn(true).when(subject).splitAvailable();
        Mockito.doReturn(0).when(subject).maxSplitSlots();
        assertFalse(subject.acceptsSplit());
        Mockito.doReturn(100).when(subject).maxSplitSlots();
        board.getPlayerSlots().get(0).setCards(new ArrayList<>());
        board.getPlayerSlots().get(0).getCards().add(new InGameCard(new Card(1, "2", Arrays.asList(2), SuitEnum.CLUBS), true));
        board.getPlayerSlots().get(0).getCards().add(new InGameCard(new Card(3, "3", Arrays.asList(3), SuitEnum.CLUBS), true));
        assertFalse(subject.acceptsSplit());
        board.getPlayerSlots().get(0).setCards(new ArrayList<>());
        board.getPlayerSlots().get(0).getCards().add(new InGameCard(new Card(1, "2", Arrays.asList(2), SuitEnum.CLUBS), true));
        board.getPlayerSlots().get(0).getCards().add(new InGameCard(new Card(1, "2", Arrays.asList(2), SuitEnum.CLUBS), true));
    }

    @Test
    public void acceptsSurrender() {
        Mockito.doReturn(false).when(subject).surrenderAvailable();
        assertFalse(subject.acceptsSurrender());
        Mockito.doReturn(true).when(subject).surrenderAvailable();
        board.getPlayerSlots().get(0).setCards(new ArrayList<>());
        board.getPlayerSlots().get(0).getCards().add(new InGameCard(new Card(1, "2", Arrays.asList(2), SuitEnum.CLUBS), true));
        board.getPlayerSlots().get(0).getCards().add(new InGameCard(new Card(3, "3", Arrays.asList(3), SuitEnum.CLUBS), true));
        board.getPlayerSlots().get(0).getCards().add(new InGameCard(new Card(3, "3", Arrays.asList(3), SuitEnum.CLUBS), true));
        assertFalse(subject.acceptsSurrender());
        board.getPlayerSlots().get(0).setCards(new ArrayList<>());
        board.getPlayerSlots().get(0).getCards().add(new InGameCard(new Card(1, "2", Arrays.asList(2), SuitEnum.CLUBS), true));
        board.getPlayerSlots().get(0).getCards().add(new InGameCard(new Card(3, "3", Arrays.asList(3), SuitEnum.CLUBS), true));
        board.setDealerSlot(new ArrayList<>());
        board.getDealerSlot().add(new InGameCard(new Card(3, "3", Arrays.asList(3), SuitEnum.CLUBS), true));
        board.getDealerSlot().add(new InGameCard(new Card(3, "3", Arrays.asList(3), SuitEnum.CLUBS), true));
        assertFalse(subject.acceptsSurrender());
        board.setDealerSlot(new ArrayList<>());
        board.getDealerSlot().add(new InGameCard(new Card(3, "3", Arrays.asList(3), SuitEnum.CLUBS), true));
        assertTrue(subject.acceptsSurrender());

    }

    @Test
    public void playerSurrender() {
        subject.playerSurrender();
        assertTrue(true);
    }

    @Test
    public void performSplit() {
        board.getPlayerSlots().get(0).setCards(new ArrayList<>());
        board.getPlayerSlots().get(0).getCards().add(new InGameCard(new Card(1, "2", Arrays.asList(2), SuitEnum.HEARTS), true));
        board.getPlayerSlots().get(0).getCards().add(new InGameCard(new Card(1, "2", Arrays.asList(2), SuitEnum.CLUBS), true));
        board.getPlayerSlots().add(new BoardSlot());
        subject.performSplit(Arrays.asList(0,1));
        assertTrue(true);
    }

    @After
    public void tearDown() throws Exception {
    }
}