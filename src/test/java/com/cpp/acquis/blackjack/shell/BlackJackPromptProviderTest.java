package com.cpp.acquis.blackjack.shell;

import com.cpp.acquis.blackjack.model.*;
import org.jline.utils.AttributedString;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class BlackJackPromptProviderTest {

    @InjectMocks
    BlackJackPromptProvider subject;

    @Mock
    Game game;

    @Test
    public void getPromptMenuTest() {
        Mockito.when(game.getStatus()).thenReturn(GameStatusEnum.MENU);
        Assert.assertEquals(subject.getPrompt(),  new AttributedString("Menu> "));
    }

    @Test
    public void getPromptStartedTest() {
        Player player = new Player("test", 300L);
        Mockito.when(game.getStatus()).thenReturn(GameStatusEnum.STARTED);
        Mockito.when(game.getPlayer()).thenReturn(player);
        Assert.assertEquals(subject.getPrompt(),  new AttributedString("Playing as test, founds 300$ > "));
    }

    @Test
    public void getPromptRoundTest() {
        Mockito.when(game.getStatus()).thenReturn(GameStatusEnum.INSURANCE);
        Player player = new Player("dummy", 100L);
        Mockito.when(game.getPlayer()).thenReturn(player);
        Board board = new Board();
        BoardSlot slot1 = new BoardSlot();
        slot1.setBet(6L);
        BoardSlot slot2 = new BoardSlot();
        slot2.setBet(2L);
        board.setPlayerSlots(Arrays.asList(slot1, slot2));
        Assert.assertEquals(subject.getPrompt(),  new AttributedString("Founds 100$> "));
    }
}