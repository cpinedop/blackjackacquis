package com.cpp.acquis.blackjack.shell;

import com.cpp.acquis.blackjack.exception.BetUnderMinimumException;
import com.cpp.acquis.blackjack.exception.InsuranceNotValidException;
import com.cpp.acquis.blackjack.model.*;
import com.cpp.acquis.blackjack.service.impl.BlackJackDealer;
import com.cpp.acquis.blackjack.service.impl.ConsoleWriterService;
import com.cpp.acquis.blackjack.service.impl.GameService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class RoundComponentTest {

    @InjectMocks
    RoundComponent subject;

    @Mock
    private Game game;

    @Mock
    private ConsoleWriterService console;

    @Mock
    private BlackJackDealer blackJackDealer;

    @Mock
    private GameService gameService;

    Board board;

    Round round;

    Player player;

    @Before
    public void setUp() throws Exception {
        board = new Board();
        board.getPlayerSlots().get(0).setBet(20L);
        Mockito.doReturn(board).when(game).getBoard();
        round = new Round();
        round.setSlot(0);
        Mockito.doReturn(round).when(game).getRound();
        player = new Player("test", 400L);
    }

    @Test
    public void bet() throws BetUnderMinimumException {
        subject.bet(30L);
        subject.bet(null);
        assertTrue(true);
    }

    @Test(expected = BetUnderMinimumException.class)
    public void betError() throws BetUnderMinimumException {
        Mockito.doReturn(10L).when(blackJackDealer).minimumBet();
        subject.bet(1L);
    }

    @Test
    public void betAvailability() {
        Mockito.doReturn(GameStatusEnum.MENU).when(game).getStatus();
        assertFalse(subject.betAvailability().isAvailable());
        Mockito.doReturn(GameStatusEnum.STARTED).when(game).getStatus();
        assertTrue(subject.betAvailability().isAvailable());
    }

    @Test
    public void insurance() throws InsuranceNotValidException {
        subject.insurance(30L);
        subject.insurance(null);
        assertTrue(true);
    }

    @Test(expected = InsuranceNotValidException.class)
    public void insuranceFail() throws InsuranceNotValidException {
        subject.insurance(2L);
        assertTrue(true);
    }

    @Test
    public void insuranceAvailability() {
        Mockito.doReturn(GameStatusEnum.MENU).when(game).getStatus();
        assertFalse(subject.insuranceAvailability().isAvailable());
        Mockito.doReturn(GameStatusEnum.INSURANCE).when(game).getStatus();
        assertTrue(subject.insuranceAvailability().isAvailable());
    }

    @Test
    public void hit() {
        subject.hit();
        assertTrue(true);
    }

    @Test
    public void hitAvailability() {
        Mockito.doReturn(GameStatusEnum.MENU).when(game).getStatus();
        assertFalse(subject.hitAvailability().isAvailable());
        Mockito.doReturn(GameStatusEnum.PLAYER_TURN).when(game).getStatus();
        assertTrue(subject.hitAvailability().isAvailable());
    }

    @Test
    public void stand() {
        subject.stand();
        assertTrue(true);
    }

    @Test
    public void standAvailability() {
        Mockito.doReturn(GameStatusEnum.MENU).when(game).getStatus();
        assertFalse(subject.standAvailability().isAvailable());
        Mockito.doReturn(GameStatusEnum.PLAYER_TURN).when(game).getStatus();
        assertTrue(subject.standAvailability().isAvailable());
    }

    @Test
    public void doubleDown() {
        subject.doubleDown();
        assertTrue(true);
    }

    @Test
    public void doubleDownAvailability() {
        Mockito.doReturn(GameStatusEnum.MENU).when(game).getStatus();
        assertFalse(subject.doubleDownAvailability().isAvailable());
        Mockito.doReturn(GameStatusEnum.PLAYER_TURN).when(game).getStatus();
        Mockito.doReturn(true).when(blackJackDealer).acceptsDoubleDown();
        assertTrue(subject.doubleDownAvailability().isAvailable());
    }

    @Test
    public void split() {
        subject.split();
        assertTrue(true);
    }

    @Test
    public void splitAvailability() {
        Mockito.doReturn(GameStatusEnum.MENU).when(game).getStatus();
        assertFalse(subject.splitAvailability().isAvailable());
        Mockito.doReturn(GameStatusEnum.PLAYER_TURN).when(game).getStatus();
        Mockito.doReturn(true).when(blackJackDealer).acceptsSplit();
        assertTrue(subject.splitAvailability().isAvailable());
    }

    @Test
    public void surrender() {
        subject.surrender();
        assertTrue(true);
    }

    @Test
    public void surrenderAvailability() {
        Mockito.doReturn(GameStatusEnum.MENU).when(game).getStatus();
        assertFalse(subject.surrenderAvailability().isAvailable());
        Mockito.doReturn(GameStatusEnum.PLAYER_TURN).when(game).getStatus();
        Mockito.doReturn(true).when(blackJackDealer).acceptsSurrender();
        assertTrue(subject.surrenderAvailability().isAvailable());
    }

    @Test
    public void show() {
        subject.show();
        assertTrue(true);
    }

    @Test
    public void showAvailability() {
        Mockito.doReturn(GameStatusEnum.MENU).when(game).getStatus();
        assertFalse(subject.showAvailability().isAvailable());
        Mockito.doReturn(GameStatusEnum.PLAYER_TURN).when(game).getStatus();
        assertTrue(subject.showAvailability().isAvailable());
    }
}