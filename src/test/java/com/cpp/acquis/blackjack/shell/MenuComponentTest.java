package com.cpp.acquis.blackjack.shell;

import com.cpp.acquis.blackjack.model.*;
import com.cpp.acquis.blackjack.service.IConsoleWriterService;
import com.cpp.acquis.blackjack.service.impl.GameService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.Availability;

import javax.annotation.Resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class MenuComponentTest {

    @InjectMocks
    MenuComponent subject;

    @Mock
    IConsoleWriterService console;

    @Mock
    Game game;

    @Mock
    GameService gameService;

    @Test
    public void start() {
        subject.start("test", 300L);
        assertTrue(true);
    }

    @Test
    public void startAvailability() {
        Mockito.doReturn(GameStatusEnum.MENU).when(game).getStatus();
        assertTrue(subject.startAvailability().isAvailable());
        Mockito.doReturn(GameStatusEnum.STARTED).when(game).getStatus();
        assertFalse(subject.startAvailability().isAvailable());
    }

    @Test
    public void end() {
        subject.end();
        assertTrue(true);
    }

    @Test
    public void endGameAvailability() {
        Mockito.doReturn(GameStatusEnum.MENU).when(game).getStatus();
        assertFalse(subject.endGameAvailability().isAvailable());
        Mockito.doReturn(GameStatusEnum.STARTED).when(game).getStatus();
        assertTrue(subject.endGameAvailability().isAvailable());
    }
}