# What is this project?
Console appplication implementing the basic rules of [BlackJack](https://www.pagat.com/banking/blackjack.html#objective)

# Run
## With docker
1. Compile: 
``` bash
mvn clean package
```
2. Go to target folder: 
``` bash 
cd target
```
3. Execute build command: 
``` bash
build.bat 
```
4. Execute run command: 
``` bash 
run.bat
```

## With Java
1. Compile: 
``` bash 
mvn clean package
```
2. Go to target folder: 
``` bash 
cd target
```
2. Execute java application: 
``` bash 
java -jar blackjack-1.0.jar
```
 
# How to play
- You can type "help" at any moment in order to show a list of the available commands.
- You can type "help \<command\>" to show how to use a command, i.e. to show help for bet: help b
- First of all you have to start a new game with the command start.

# Tested environment
- Windows 10
- Docker version 18.09.2
- jdk-10.0.2
- maven-3.5.4

# Notes
[Lombok project](https://projectlombok.org/) has been used in this development. A plugin for each IDE may be needed to review the code properly.
- [Eclipse Lombok plugin](https://projectlombok.org/setup/eclipse)
- [IntelIj Lombok plugin](https://projectlombok.org/setup/intellij)
- ...

